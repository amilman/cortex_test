
import urllib.request, urllib.parse
from queue import Queue
from multiprocessing import Pool
import random
import time
import http.cookiejar

from pyquery import PyQuery as pq

USER_AGENTS = ['Mozilla/5.0 (X11; U; Linux x86_64; en-US) AppleWebKit/540.0 (KHTML,like Gecko) Chrome/9.1.0.0 Safari/540.0',
               'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.27 Safari/537.17',
               'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Ubuntu/11.10 Chromium/16.0.912.21 Chrome/16.0.912.21 Safari/535.7',
               'Opera/9.80 (X11; Linux i686; U; ru) Presto/2.8.131 Version/11.11',
               'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11) AppleWebKit/601.1.39 (KHTML, like Gecko) Version/9.0 Safari/601.1.39']

USER_AGENT = "Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.27 Safari/537.17"
GOOGLE_URL = 'http://www.google.com/search?q=%s&num=3&lr=lang_en'

def query_google(query, sor = False):
    headers = {}
    ua_index = random.randrange(len(USER_AGENTS))
    headers['User-Agent'] = USER_AGENTS[ua_index]
    url = GOOGLE_URL % urllib.parse.quote(query)
    res = 'NO RESULT'
    succeed = True
    if sor:
        time.sleep(random.random())
    try:
        cj = http.cookiejar.CookieJar()
        opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor(cj))
        req = urllib.request.Request(url, headers = headers)
        resp = opener.open(req)
        data = resp.read()
        res = pq(pq(data)('div#ires div.g')[0]).html()
    except Exception as e:
        res = '<p>%s</p>' % str(e)
        succeed = False
    return query, res, succeed

class GoogleSearcher:

    def __init__(self, search_list, processes = 4):
        self.processes = processes
        self.search_list = search_list
        self.results_queue = Queue()

    def push_result(self, res):
        self.results_queue.put(res)
        
    def __call__(self):
        pool = Pool(self.processes)
        for query in self.search_list:
            pool.apply_async(query_google, args = (query, ), callback = self.push_result)
        pool.close()
        pool.join()

    def __len__(self):
        return len(self.search_list)
