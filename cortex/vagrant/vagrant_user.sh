#!/usr/bin/env bash

mkdir -p /home/vagrant/.venv
pyvenv /home/vagrant/.venv/cortex_test
source /home/vagrant/.venv/cortex_test/bin/activate
pip install --upgrade pip
pip install setuptools flask pyquery uwsgi wtforms
cd /home/vagrant/cortex_test/cortex
uwsgi -s /tmp/uwsgi.sock --manage-script-name --mount /=app:app -d /tmp/uwsgi.log
