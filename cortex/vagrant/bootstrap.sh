#!/usr/bin/env bash

sudo apt-get update
sudo apt-get install -y nginx
sudo apt-get -y install python-pip python3-venv python3-dev git libxml2 libxml2-dev libxslt1-dev python-pyquery zlib1g-dev

cd /home/vagrant
sudo -H -u vagrant git clone https://amilman@bitbucket.org/amilman/cortex_test.git
sudo -H -u vagrant bash /home/vagrant/cortex_test/cortex/vagrant/vagrant_user.sh

sudo rm /etc/nginx/sites-enabled/default
sudo cp /home/vagrant/cortex_test/cortex/nginx/cortex_test /etc/nginx/sites-available/
sudo ln -s /etc/nginx/sites-available/cortex_test /etc/nginx/sites-enabled/
sudo service nginx restart
