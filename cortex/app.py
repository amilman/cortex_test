
import os
from threading import Thread
import html
import json
import urllib.parse
import flask
from flask import render_template, request, redirect, url_for
from werkzeug import secure_filename
import wtforms

from google_searcher import GoogleSearcher

UPLOAD_FOLDER = '/tmp/'
ALLOWED_EXTENSIONS = set(['txt', 'lst', 'csv'])

app = flask.Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def format_result(query, result, succeed):
    ptype = 'success' if succeed else 'danger'
    return '''<div class="panel panel-%s">
    <div class="panel-heading">
        <h3 class="panel-title">%s</h3>
    </div>
    <div class="panel-body">
        %s
    </div>
</div>''' % (ptype, query, result) 
    
def event_stream(gsearcher, bulk_size = 4):
    remains = len(gsearcher)
    search_thread = Thread(target = gsearcher)
    search_thread.start()
    while remains > 0:
        bulk = []
        bsize = min(bulk_size, remains)
        for i in range(bsize):
            query, res, succeed = gsearcher.results_queue.get()
            result = format_result(query, res, succeed)
            bulk.append(result)
        remains -= bsize
        res = '\n'.join(bulk).strip()
        d = 'data: %s\n\n' % json.dumps(res)
        yield d
    yield 'data: %s\n\n' % json.dumps('CLOSE')

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

class RequestForm(wtforms.Form):
    file = wtforms.FileField('CSVFile')
    max_requestes = wtforms.IntegerField(default = 20)
    proc_num = wtforms.IntegerField(default = 4)
    bulk_size = wtforms.IntegerField(default = 4)
    sleep_on_request = wtforms.BooleanField(default = False)
    

@app.route('/', methods=['GET', 'POST'])
def index():
    form = RequestForm(request.form)
    if request.method == 'POST':
        file = request.files['file']
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            file.save(file_path)
            fdata = {'fn': filename,
                     'mrq': form.max_requestes.data,
                     'prcn': form.proc_num.data,
                     'bsz': form.bulk_size.data,
                     'sor': form.sleep_on_request.data}            
            return render_template('index.html', form = form, uquery = urllib.parse.urlencode(fdata))
    return render_template('index.html', form = form)

@app.route('/stream')
def stream():
    #print(request.args)
    filename = request.args.get('fn')
    bulk_size = int(request.args.get('bsz') or 4)
    proc_num = int(request.args.get('prcn') or 4)
    mrq = int(request.args.get('mrq', 20))
    if filename:
        file_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
        f = open(file_path, 'r')
        l = f.read().split(',')
        if mrq:
            l = l[:mrq]
            gs = GoogleSearcher(l, proc_num)
        return flask.Response(event_stream(gs, bulk_size),
                              mimetype="text/event-stream")
    
if __name__ == '__main__':
    app.debug = True
    app.run(threaded=True)
